import numpy as np

class Vertices:
    #Erstellt einen Array als Bereich
    DEFAULT = np.array([[900,450],[1100,450],[1695,850],[1000,975],[250,850]], np.int32)
    ETS = np.array([[850+17,430],[1100+34,430],[1400,675],[600-1,675]], np.int32)
    #GTA = np.array([[900,450],[1100,450],[1695,850],[1000,975],[250,850]], np.int32)
    UNITY = np.array([[900,450],[1100,450],[1695,850],[1000,975],[250,850]], np.int32)
    GOOGLE = np.array([[900,450],[1100,450],[1695,850],[1000,975],[250,850]], np.int32)
    FILE = np.array([[900,450],[1100,450],[1695,850],[1000,975],[250,850]], np.int32)

class Windownames:
    ETS = "Euro Truck Simulator 2"
    #GTA = ""
    UNITY = "SimpleCarGame"
    GOOGLE = ""
    FILE = ""