import numpy as np
from PIL import ImageGrab
import cv2
import time
import math
import Presets
from pynput import keyboard
import Keys
from win32gui import GetWindowText, GetForegroundWindow
import tkinter as tk
from tkinter import filedialog

class ImageRecognition:
    

    # Nimmt den Bildschirm auf -> Das Fenster 
    def capture_screen(self):
        screen = np.array(ImageGrab.grab(bbox=(0,0,1920,1080)))
        screen = cv2.cvtColor(screen, cv2.COLOR_BGR2RGB)
        return screen

    def stretch_image(self, image):
        #4 ausgewählte Positionen des Bildes = Input
        pts1 = np.array([[250,450],[1695,450],[1695,975],[250,975]], np.float32)
        #Die Punkte sollen auf diese Koordinaten verschoben werden -> Damit auch das Bild = Output
        pts2 = np.array([[0,0],[1920,0],[1920,1080],[0,1080]], np.float32)
        #Erstellt anhand der Punkte eine 2*3 Matrix
        M_perspective = cv2.getPerspectiveTransform(pts1, pts2)
        # Ändert die Perspektive mit einer 3*3 Matrix, daher ist der letzte Punkt die Ecke des Bildschirms 
        image = cv2.warpPerspective(image, M_perspective, (1920,1080))

        #cv2.imshow("stretched", cv2.resize(image, (0,0), fx=0.5, fy=0.5))

        return image
        # Mehr erfahren: https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_geometric_transformations/py_geometric_transformations.html

    def unstretch_image(self, image):
        # Siehe streth_image
        pts1 = np.array([[0,0],[1920,0],[1920,1080],[0,1080]], np.float32)
        pts2 = np.array([[250,450],[1695,450],[1695,975],[250,975]], np.float32)

        M_perspective = cv2.getPerspectiveTransform(pts1, pts2)
        image = cv2.warpPerspective(image, M_perspective, (1920,1080))
        return image

    def roi(self, image, vertices): # Region-Of-Interest
        # Maskenarray mit 0 - selbe Größe wie das Bild, welches übergeben wurde 
        mask = np.zeros_like(image)
        #Ein Polygon auf der Maske der Größe Verticles mit den Farben wird erstellt 
        cv2.fillPoly(mask, [vertices], [255,255,255])
        #Nur noch die Maske anzeigen 
        masked_image = cv2.bitwise_and(mask, image)
        return masked_image

    def white(self, image, threshold): #threshold = 100 (Z:132)
        #Konvertiert die Farben des übergebenen Bildes in HSV 
        hsv_image = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)

        #Definieren des Bereiches der Farbe Weiß
        lower_white = np.array([0, 0, 255-threshold], dtype=np.uint8)
        upper_white = np.array([255, threshold ,255], dtype=np.uint8)
        #Nur noch Weiß erhalten + Maske auf das Bild 
        mask = cv2.inRange(hsv_image, lower_white, upper_white)
        white_image = cv2.bitwise_and(image, image, mask= mask)

        white_image = cv2.cvtColor(cv2.cvtColor(white_image, cv2.COLOR_HSV2RGB),cv2.COLOR_RGB2GRAY)

        return white_image
        #Mehr erfahren: https://de.wikipedia.org/wiki/HSV-Farbraum , https://alloyui.com/examples/color-picker/hsv.html


    def canny(self, image):
        #Edge (Kante/Ecke?) 
        image = cv2.Canny(image, 200, 600)
        #cv2.imshow("canny", cv2.resize(image, (0,0), fx=0.5, fy=0.5))
        return image
        #Mehr: https://towardsdatascience.com/canny-edge-detection-step-by-step-in-python-computer-vision-b49c3a2d8123

    def lines(self, original_image, image):
        #Maske
        line_image = np.zeros_like(original_image)
        #Erstellt einen Array(,) aus Distanz einer Line zum Ursprung + den Winkel 
        # Info: https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_houghlines/py_houghlines.html
        lines = cv2.HoughLinesP(image, 2, np.pi/180, 100, np.array([]), minLineLength=40, maxLineGap=5)

        valid_lines = []
        left_lines = []
        right_lines = []
        lane_markers = []
        left_lane_marker_exists = False
        right_lane_marker_exists = False
        points = []
        lines2 = None
        if lines is not None:
            for line in lines:
                x1,y1,x2,y2 = line.reshape(4)
                if abs(self.slope(line)) > (1/3) and (math.isnan(self.slope(line)) is not True and (abs(self.slope(line)))<250):
                    valid_lines.append(line)

            point_image = np.zeros_like(original_image)
            pre_lines_image = np.zeros_like(original_image)

            for valid_line in valid_lines:
                x1,y1,x2,y2 = valid_line.reshape(4)
                cv2.line(pre_lines_image, (x1,y1), (x2,y2), [255,255,255], 1)
                #points.append(self.get_points_of_line(valid_line)[0])
            #print(points)
            # for point in points:
            #     point = np.int32(point)
            #     cv2.circle(point_image, tuple(point), 5, (255,0,0), -1)
            #cv2.imshow("pre_lines", cv2.resize(pre_lines_image, (0,0), fx=0.5, fy=0.5))

            pre_lines_image = self.canny(pre_lines_image)

            # pre_lines_image = cv2.cvtColor(pre_lines_image, cv2.COLOR_BGR2GRAY)


            lines2 = cv2.HoughLinesP(pre_lines_image, 2, np.pi/180, 100, np.array([]), minLineLength=100, maxLineGap=10)
            valid_lines = []


        if lines2 is not None:
            for line2 in lines2:
                x1,y1,x2,y2 = line2.reshape(4)
                if abs(self.slope(line2)) > (1/3) and (math.isnan(self.slope(line2)) is not True and (abs(self.slope(line2)))<250):
                    #Zeichnet Line 
                    if(self.slope(line2)>0):
                        cv2.line(line_image, (x1,y1), (x2,y2), [255,100,100], 10)
                        right_lines.append((self.slope(line2), self.y_intercept(line2, x1, y1)))

                    elif(self.slope(line2)<0):
                        cv2.line(line_image, (x1,y1), (x2,y2), [255,100,0], 10)
                        left_lines.append((self.slope(line2), self.y_intercept(line2, x1, y1)))


        right_lane_marker = np.array([[0,0],[0,0]] ,np.int32)
        left_lane_marker = np.array([[0,0],[0,0]], np.int32)

        if len(left_lines) != 0:
            left_lines_average = np.average(left_lines, axis=0)
            left_lane_marker = self.lane_marker(image, left_lines_average)
            left_lane_marker_exists = True
            lane_markers.append(left_lane_marker)
            cv2.line(line_image, left_lane_marker[0], left_lane_marker[1], [255,0,255], 20)


        if len(right_lines) != 0:
            right_lines_average = np.average(right_lines, axis=0)
            right_lane_marker = self.lane_marker(image, right_lines_average)
            right_lane_marker_exists = True
            lane_markers.append(right_lane_marker)
            cv2.line(line_image, right_lane_marker[0], right_lane_marker[1], [0,255,0], 20)

        # if(left_lane_marker_exists and right_lane_marker_exists):
        x1 = int((right_lane_marker[0][0] + left_lane_marker[0][0]) / 2)
        x2 = int((right_lane_marker[1][0] + left_lane_marker[1][0]) / 2)
        y1 = right_lane_marker[0][1]
        y2 = right_lane_marker[1][1]
        cv2.line(line_image, (x1,y1), (x2,y2), [0,0,255], 25)
        center_line = np.array([[x1,y1,x2,y2]])


        if ("center_line" in locals()):
            return center_line, line_image, True

        else:
            print("returning default ceter line")
            return np.array([[960, 540, 960, 1080]]), line_image, False

    def slope(self, line, mode="default"):
        x1,y1,x2,y2 = line.reshape(4)

        #y = mx + b
        if(mode!="default"):
            if(x1==x2):
                x2 += 1
        m = (y2-y1)/(x2-x1)
        return m

    def y_intercept(self, line, x,y):
        #y = mx + b
        #b = y - mx
        b = y-(self.slope(line)*x)
        return b

    def lane_marker(self, image, line_params):
        try:
            slope, y_intercept = line_params.reshape(2)

            y1 = image.shape[0]
            y2 = int(y1*(2/5))
            x1 = int((y1-y_intercept)/slope)
            x2 = int((y2-y_intercept)/slope)
            return [(x1,y1),(x2,y2)]
        except:
            print("returning default lane marker")
            return [(960, 1080), (960, 792)]

    def get_points_of_line(self, line):
        x1,y1,x2,y2 = line.reshape(4)
        #https://stackoverflow.com/questions/32328179/opencv-3-0-lineiterator/59525697#59525697
        pt1 = np.array([x1,y1], np.int32)
        pt2 = np.array([x2,y2], np.int32)
        points_on_line = np.linspace(pt1, pt2, np.linalg.norm(pt1-pt2))
        return points_on_line



    def apply_input(self, slope, steering_threshold, steering_intensity):
        if(slope > steering_threshold):

            #links
            Keys.PressKey(Keys.A)
            time.sleep(steering_intensity)
            Keys.ReleaseKey(Keys.A)
        
        elif(slope<(-steering_threshold)):
            #rechts
            Keys.PressKey(Keys.D)
            time.sleep(steering_intensity)
            Keys.ReleaseKey(Keys.D)

    def get_file(self):
        file = filedialog.askopenfilename(filetypes=[("PNG", ".png")])
        return file

    def run(self, choice):

        steering_offset = 0 #höher: zieht nach links
        steering_intensity = 0.05 #In sekunden

        require_file = False

        if(choice=="ETS"):
            vertices = Presets.Vertices.ETS
            window_name = Presets.Windownames.ETS
            require_key_input = True
        
        # elif(choice=="GTA"):
        #     vertices = Presets.Vertices.GTA
        #     window_name = Presets.Windownames.GTA
        #     require_key_input = True

        elif(choice=="UNITY"):
            vertices = Presets.Vertices.UNITY
            window_name = Presets.Windownames.UNITY
            require_key_input = True
            steering_intensity = 0.07

        elif(choice=="GOOGLE"):
            vertices = Presets.Vertices.GOOGLE
            window_name = Presets.Windownames.GOOGLE
            require_key_input = False

        elif(choice=="FILE"):
            vertices = Presets.Vertices.FILE
            window_name = Presets.Windownames.FILE
            require_key_input = False
            require_file = True
            file = self.get_file()
            if(file==''):
                return
       
       
       
        starting_time = time.time()

        white_threshold = 100 # zwischen 0 und 255
        steering_threshold = 0.1
        found_line = False


        while(True):
            if(require_file):
                original_image = cv2.imread(file)
            else:
                original_image = self.capture_screen()
            #original_image = cv2.imread("res/Street2.png")
            
            original_image = cv2.resize(original_image, (0,0), fx=(float(1920/original_image.shape[1])), fy=float((1080/original_image.shape[0])))

            image = original_image
            image = self.white(image, white_threshold)
            image = self.canny(image)
            image = self.roi(image, vertices)
            image = self.stretch_image(image)
            center_line, line_image, found_line = self.lines(original_image, image)

            if (require_key_input):
                focused_window = GetWindowText(GetForegroundWindow())
                if(focused_window == window_name):
                    try:
                        road_slope = math.tan(self.slope(center_line, mode="exceptional"))
                        if(found_line):
                            road_slope += steering_offset
                            found_line = False
                        
                        self.apply_input(road_slope, steering_threshold, steering_intensity)
                
                    except Exception as e:
                        print(e)

            image = self.unstretch_image(image)
            unstretched_lines = self.unstretch_image(line_image)

            #cv2.imshow("unstretched_lines", cv2.resize(unstretched_lines, (0,0), fx=0.5, fy=0.5))

            image = cv2.addWeighted(unstretched_lines, 1, original_image, 0.8, 1)

            # print("{} calculations per second" .format(1/(time.time()-starting_time)))
            # starting_time = time.time()

            cv2.putText(image, "Press Escape to close this Window", (600, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,0), 9)
            cv2.putText(image, "Press Escape to close this Window", (600, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 6)

            #cv2.imshow("window", cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
            resized_processed = cv2.resize(image, (0,0), fx=0.5, fy=0.5)
            resized_original = cv2.resize(original_image, (0,0), fx=0.5, fy=0.5)


            cv2.imshow("processed", resized_processed)
            #cv2.imshow("original", resized_original)

            k = cv2.waitKey(33)
            if k==27:    # Esc key to stop
                cv2.destroyAllWindows()
                break

    def __init__(self):
        pass
# Leitet Programm ein
if __name__ == "__main__":
    pass
    #listener = keyboard.Listener(on_press=on_press)
    #ir = ImageRecognition()