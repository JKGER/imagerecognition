import tkinter as tk
from PIL import ImageTk, Image
import webbrowser
from Main import ImageRecognition

"""
1. Message content -> http://effbot.org/tkinterbook/message.htm
2. 3 Knöpfe mit Bildern : ETS, GTA, Weitere -> https://www.geeksforgeeks.org/python-add-image-on-a-tkinter-button/ (500x253)
3. Confirmlog = Message
4. Schließen
"""


class Window:
    """
    Diese Klasse ist für das Startfenster zuständig
    """
    finished = False
    choice = ""


    def onclickEts(self):
        """
        Setzt den Werte fest 
        """
        self.choice = "ETS"
        print("Successfully added {}" .format(self.choice))
        finished = True
        self.start_main_script(self.choice)
      

    # def onclickGta(self):
    #     """
    #     Wählt Gta als Spiel aus
    #     """
    #     self.choice = "GTA"
    #     print("Successfully added {}" .format(self.choice))
    #     finished = True
    #     self.start_main_script(self.choice)

    def onclickUnity(self):
        """
        Wählt Unity aus 
        """
        self.choice = "UNITY"
        print("Sucessfully added {}" .format(self.choice))
        finished = True
        self.start_main_script(self.choice)

    def onclickGoogle(self):
        """
        Google als Auswal
        """
        self.choice = "GOOGLE"
        print("Successfully added {}" .format(self.choice))
        finished = True
        self.start_main_script(self.choice)

    def onclickFile(self):
        """
        Eigene Datei als Auswahl als Auswal
        """
        self.choice = "FILE"
        print("Successfully added {}" .format(self.choice))
        finished = True
        self.start_main_script(self.choice)


    def start_main_script(self, choice):
        ir = ImageRecognition()
        ir.run(choice)

    def __init__(self):

        root = tk.Tk(className=" Image Recognition")
        root.geometry('1020x600')
        root.configure(bg="black")
        root.resizable(0,0)

        #Content
        top_frame = tk.Frame(root, bg="black", width="750", height="375")
        middle_frame = tk.Frame(root,bg="black")
        bottom_frame = tk.Frame(root, bg="black")
    
        #Widget
        text = tk.Label(top_frame, text="|------Select preferred Game------|", font="Helvetica", bg="black", fg="white")
        link1 = tk.Label(bottom_frame, text="ImageRecognition GitLab Repository", fg="blue",bg="black", cursor="hand2")

        #Image 
        ets_photo = ImageTk.PhotoImage(Image.open("res/ETSCover.jpg"))  # PIL solution
        #gta_photo = ImageTk.PhotoImage(Image.open("res/GtaCover.jpg"))  # PIL solution
        unity_photo = ImageTk.PhotoImage(Image.open("res/UnityCover.jpg"))  # PIL solution
        google_photo = ImageTk.PhotoImage(Image.open("res/GoogleMapsLogo.jpg"))  # PIL solution
        file_photo = ImageTk.PhotoImage(Image.open("res/File.jpg"))

        #Button
        etsbutton = tk.Button(middle_frame, text="Hier drücken", image=ets_photo, command=self.onclickEts)
        #gtabutton = tk.Button(middle_frame, text="Hier drücken", image=gta_photo, command=self.onclickGta)
        unitybutton = tk.Button(middle_frame, text="Hier drücken", image=unity_photo, command=self.onclickUnity)
        googlebutton = tk.Button(middle_frame, text="Hier drücken", image=google_photo, command=self.onclickGoogle)
        filebutton = tk.Button(middle_frame, text="Hier drücken", image=file_photo, command=self.onclickFile)

        #Pack
        top_frame.grid(column=0,row=0)
        middle_frame.grid(column=0,row=1)
        bottom_frame.grid(column=0,row=2)

        link1.grid(column=1,row=3,sticky=tk.S)
        link1.bind("<Button-1>", lambda e: callback("http://www.google.com"))
        text.grid(column=0,row=1)

        etsbutton.grid(column=1,row=1)  
        unitybutton.grid(column=1,row=2) 
        #gtabutton.grid(column=2,row=1) 
        googlebutton.grid(column=2,row=1)
        filebutton.grid(column=2, row=2)

        root.mainloop()
        return

if __name__ == "__main__":
    window = Window()
