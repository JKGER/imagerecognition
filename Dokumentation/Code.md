# Nicht erläuterte Dokumentation des Codes

Hinweis: Diese Seite enthält nur eine unkommentierte Version aller Funktionen und den vollständigen unkommentierten Quellcode. Für die Projektseite [hier](Projektseite.md) klicken.

## Inhalt

- 1 [Klassen](#klassen)

- 1.1 [Window](#window)

- 1.2 [ImageRecognition](#imagerecognition)

- 2 [Vollständiger Quelltext von ImageRecognition](#quelltext)

# Klassen <a name="klassen">

## Window <a name="window">

### \_\_init__

Erstellt ein Fenster, welches als Launcher funktioniert.

```python
def __init__(self):
```

Parameter:

- /

### onclickEts

Setzt die Auswahl auf "ETS".

```python
def onclickEts(self):
```

Parameter:

- /

### onclickUnity

Setzt die Auswahl auf "UNITY".

```python
def onclickUnity(self):
```

Parameter:

- /

### onclickGoogle

Setzt die Auswahl auf "GOOGLE".

```python
def onclickGoogle(self):
```

Parameter:

- /

### onclickFile

Setzt die Auswahl auf "FILE".

```python
def onclickFile(self):
```

Parameter:

- /

### start_main_script

Startet den eigentlichen Code.

```python
def start_main_script(self, choice):
```

Parameter:

- choice: String

---

## ImageRecognition <a name="imagerecognition">

### run

Initialisiert und enthält den Kern des Codes.

```python
def run(self, choice):
```

Parameter:

- choice: String

### get_file()

Gibt den Pfad einer vom Benutzer ausgewählten Datei zurück.

```python
def get_file(self):
```

Parameter:

- /

### capture_screen

Gibt einen Screenshot im RGB-Format zurück.

```python
def capture_screem(self):
```

Parameter:

- /

### white

Filtert alle nicht-weißen Teile aus einem Bild heraus.

```python
def white(self, image, threshold):
```

Parameter:

- image: Bild im RGB-Format
- threshold: Integer zwischen 0 und 255

### canny

Durchsucht ein Schwarz-Weiß Bild nach Kanten.

```python
def canny(self, image):
```

Parameter:

- image: Bild im GRAY-Format

### roi

Schneidet einen Teil des Bildes heraus.

```python
def roi(self, image, vertices):
```

Parameter:

- image: Bild

- vertices: np.Array()

### stretch_image

Streckt das Bild.

```python
def stretch_image(self, image):
```

Parameter:

- image: Bild

### lines

Gibt die Mittlere Linie, ein Bild mit Linien und einen Boolean zurück, der angibt ob eine Linie gefunden wurde.

```python
def lines(self, original_image, image):
```

Parameter:

- orginal_image: Ursprüngliches Bild

- image: zu durchsuchendes Bild

### slope

Gibt die Steigung einer Gerade durch zwei Punkte zurück.

```python
def slope(self, line, mode="default"):
```

Parameter:

- line: Tuple mit zwei Koordinaten der Linie

- mode: String; wenn nicht "default", dann gibt es auch bei unendlichen Steigung eine Lösung

### y_intercept

Gibt den y-Achsenschnittpunkt einer Gerade durch zwei Punkte zurück.

```python
def y_intercept(self, line, x, y):
```

Parameter:

- line: Tuple mit zwei Koordinaten der Linie

- x: Integer; X Koordinate eines beliebigen Punktes auf der Linie

- y: Integer; Y Koordinate eines beliebigen Punktes auf der Linie

### lane_marker

Gibt die Start- und Endkoordinaten einer Linie zurück.

```python
def lane_marker(self, image, line_params):
```

Parameter:

- image: gefiltertes Bild

- line_params: Tuple aus Steigung und y-Achsenschnittpunkt einer Gerade.

### apply_input

Löst Tastenanschläge aus.

```python
def apply_input(self, slope, steering_threshold, steering_intensity):
```

Parameter:

- slope: Steigung der mittleren Linie

- steering_threshold: Wert, ab dem die Taste gedückt wird.

- steering_intensity: Gibt in Sekunden an, wie lange in eine bestimmte Richtung gelenkt wird.

# Vollständiger Quelltext von ImageRecognition <a name="quelltext">

<details>

<summary>Window.py</summary>

```python
import tkinter as tk
from PIL import ImageTk, Image
import webbrowser
from Main import ImageRecognition

"""
1. Message content -> http://effbot.org/tkinterbook/message.htm
2. 3 Knöpfe mit Bildern : ETS, GTA, Weitere -> https://www.geeksforgeeks.org/python-add-image-on-a-tkinter-button/ (500x253)
3. Confirmlog = Message
4. Schließen
"""


class Window:
    """
    Diese Klasse ist für das Startfenster zuständig
    """
    finished = False
    choice = ""


    def onclickEts(self):
        """
        Setzt den Werte fest
        """
        self.choice = "ETS"
        print("Successfully added {}" .format(self.choice))
        finished = True
        self.start_main_script(self.choice)


    # def onclickGta(self):
    #     """
    #     Wählt Gta als Spiel aus
    #     """
    #     self.choice = "GTA"
    #     print("Successfully added {}" .format(self.choice))
    #     finished = True
    #     self.start_main_script(self.choice)

    def onclickUnity(self):
        """
        Wählt Unity aus
        """
        self.choice = "UNITY"
        print("Sucessfully added {}" .format(self.choice))
        finished = True
        self.start_main_script(self.choice)

    def onclickGoogle(self):
        """
        Google als Auswal
        """
        self.choice = "GOOGLE"
        print("Successfully added {}" .format(self.choice))
        finished = True
        self.start_main_script(self.choice)

    def onclickFile(self):
        """
        Eigene Datei als Auswahl als Auswal
        """
        self.choice = "FILE"
        print("Successfully added {}" .format(self.choice))
        finished = True
        self.start_main_script(self.choice)


    def start_main_script(self, choice):
        ir = ImageRecognition()
        ir.run(choice)

    def __init__(self):

        root = tk.Tk(className=" Image Recognition")
        root.geometry('1020x600')
        root.configure(bg="black")
        root.resizable(0,0)

        #Content
        top_frame = tk.Frame(root, bg="black", width="750", height="375")
        middle_frame = tk.Frame(root,bg="black")
        bottom_frame = tk.Frame(root, bg="black")

        #Widget
        text = tk.Label(top_frame, text="|------Select preferred Game------|", font="Helvetica", bg="black", fg="white")
        link1 = tk.Label(bottom_frame, text="ImageRecognition GitLab Repository", fg="blue",bg="black", cursor="hand2")

        #Image
        ets_photo = ImageTk.PhotoImage(Image.open("res/ETSCover.jpg"))  # PIL solution
        #gta_photo = ImageTk.PhotoImage(Image.open("res/GtaCover.jpg"))  # PIL solution
        unity_photo = ImageTk.PhotoImage(Image.open("res/UnityCover.jpg"))  # PIL solution
        google_photo = ImageTk.PhotoImage(Image.open("res/GoogleMapsLogo.jpg"))  # PIL solution
        file_photo = ImageTk.PhotoImage(Image.open("res/File.jpg"))

        #Button
        etsbutton = tk.Button(middle_frame, text="Hier drücken", image=ets_photo, command=self.onclickEts)
        #gtabutton = tk.Button(middle_frame, text="Hier drücken", image=gta_photo, command=self.onclickGta)
        unitybutton = tk.Button(middle_frame, text="Hier drücken", image=unity_photo, command=self.onclickUnity)
        googlebutton = tk.Button(middle_frame, text="Hier drücken", image=google_photo, command=self.onclickGoogle)
        filebutton = tk.Button(middle_frame, text="Hier drücken", image=file_photo, command=self.onclickFile)

        #Pack
        top_frame.grid(column=0,row=0)
        middle_frame.grid(column=0,row=1)
        bottom_frame.grid(column=0,row=2)

        link1.grid(column=1,row=3,sticky=tk.S)
        link1.bind("<Button-1>", lambda e: callback("http://www.google.com"))
        text.grid(column=0,row=1)

        etsbutton.grid(column=1,row=1)  
        unitybutton.grid(column=1,row=2)
        #gtabutton.grid(column=2,row=1)
        googlebutton.grid(column=2,row=1)
        filebutton.grid(column=2, row=2)

        root.mainloop()
        return

if __name__ == "__main__":
    window = Window()
```

</details>

<details>

<summary>Main.py</summary>

```python
import numpy as np
from PIL import ImageGrab
import cv2
import time
import math
import Presets
from pynput import keyboard
import Keys
from win32gui import GetWindowText, GetForegroundWindow
import tkinter as tk
from tkinter import filedialog


class ImageRecognition:


    # Nimmt den Bildschirm auf -> Das Fenster
    def capture_screen(self):
        screen = np.array(ImageGrab.grab(bbox=(0,0,1920,1080)))
        screen = cv2.cvtColor(screen, cv2.COLOR_BGR2RGB)
        return screen

    def stretch_image(self, image):
        #4 ausgewählte Positionen des Bildes = Input
        pts1 = np.array([[250,450],[1695,450],[1695,975],[250,975]], np.float32)
        #Die Punkte sollen auf diese Koordinaten verschoben werden -> Damit auch das Bild = Output
        pts2 = np.array([[0,0],[1920,0],[1920,1080],[0,1080]], np.float32)
        #Erstellt anhand der Punkte eine 2*3 Matrix
        M_perspective = cv2.getPerspectiveTransform(pts1, pts2)
        # Ändert die Perspektive mit einer 3*3 Matrix, daher ist der letzte Punkt die Ecke des Bildschirms
        image = cv2.warpPerspective(image, M_perspective, (1920,1080))

        #cv2.imshow("stretched", cv2.resize(image, (0,0), fx=0.5, fy=0.5))

        return image
        # Mehr erfahren: https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_geometric_transformations/py_geometric_transformations.html

    def unstretch_image(self, image):
        # Siehe streth_image
        pts1 = np.array([[0,0],[1920,0],[1920,1080],[0,1080]], np.float32)
        pts2 = np.array([[250,450],[1695,450],[1695,975],[250,975]], np.float32)

        M_perspective = cv2.getPerspectiveTransform(pts1, pts2)
        image = cv2.warpPerspective(image, M_perspective, (1920,1080))
        return image

    def roi(self, image, vertices): # Region-Of-Interest
        # Maskenarray mit 0 - selbe Größe wie das Bild, welches übergeben wurde
        mask = np.zeros_like(image)
        #Ein Polygon auf der Maske der Größe Verticles mit den Farben wird erstellt
        cv2.fillPoly(mask, [vertices], [255,255,255])
        #Nur noch die Maske anzeigen
        masked_image = cv2.bitwise_and(mask, image)
        return masked_image

    def white(self, image, threshold): #threshold = 100 (Z:132)
        #Konvertiert die Farben des übergebenen Bildes in HSV
        hsv_image = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)

        #Definieren des Bereiches der Farbe Weiß
        lower_white = np.array([0, 0, 255-threshold], dtype=np.uint8)
        upper_white = np.array([255, threshold ,255], dtype=np.uint8)
        #Nur noch Weiß erhalten + Maske auf das Bild
        mask = cv2.inRange(hsv_image, lower_white, upper_white)
        white_image = cv2.bitwise_and(image, image, mask= mask)

        white_image = cv2.cvtColor(cv2.cvtColor(white_image, cv2.COLOR_HSV2RGB),cv2.COLOR_RGB2GRAY)

        return white_image
        #Mehr erfahren: https://de.wikipedia.org/wiki/HSV-Farbraum , https://alloyui.com/examples/color-picker/hsv.html


    def canny(self, image):
        #Edge (Kante/Ecke?)
        image = cv2.Canny(image, 200, 600)
        #cv2.imshow("canny", cv2.resize(image, (0,0), fx=0.5, fy=0.5))
        return image
        #Mehr: https://towardsdatascience.com/canny-edge-detection-step-by-step-in-python-computer-vision-b49c3a2d8123

    def lines(self, original_image, image):
        #Maske
        line_image = np.zeros_like(original_image)
        #Erstellt einen Array(,) aus Distanz einer Line zum Ursprung + den Winkel
        # Info: https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_houghlines/py_houghlines.html
        lines = cv2.HoughLinesP(image, 2, np.pi/180, 100, np.array([]), minLineLength=40, maxLineGap=5)

        valid_lines = []
        left_lines = []
        right_lines = []
        lane_markers = []
        left_lane_marker_exists = False
        right_lane_marker_exists = False
        points = []
        lines2 = None
        if lines is not None:
            for line in lines:
                x1,y1,x2,y2 = line.reshape(4)
                if abs(self.slope(line)) > (1/3) and (math.isnan(self.slope(line)) is not True and (abs(self.slope(line)))<250):
                    valid_lines.append(line)

            point_image = np.zeros_like(original_image)
            pre_lines_image = np.zeros_like(original_image)

            for valid_line in valid_lines:
                x1,y1,x2,y2 = valid_line.reshape(4)
                cv2.line(pre_lines_image, (x1,y1), (x2,y2), [255,255,255], 1)
                #points.append(self.get_points_of_line(valid_line)[0])
            #print(points)
            # for point in points:
            #     point = np.int32(point)
            #     cv2.circle(point_image, tuple(point), 5, (255,0,0), -1)
            #cv2.imshow("pre_lines", cv2.resize(pre_lines_image, (0,0), fx=0.5, fy=0.5))

            pre_lines_image = self.canny(pre_lines_image)

            # pre_lines_image = cv2.cvtColor(pre_lines_image, cv2.COLOR_BGR2GRAY)


            lines2 = cv2.HoughLinesP(pre_lines_image, 2, np.pi/180, 100, np.array([]), minLineLength=100, maxLineGap=10)
            valid_lines = []


        if lines2 is not None:
            for line2 in lines2:
                x1,y1,x2,y2 = line2.reshape(4)
                if abs(self.slope(line2)) > (1/3) and (math.isnan(self.slope(line2)) is not True and (abs(self.slope(line2)))<250):
                    #Zeichnet Line
                    if(self.slope(line2)>0):
                        cv2.line(line_image, (x1,y1), (x2,y2), [255,100,100], 10)
                        right_lines.append((self.slope(line2), self.y_intercept(line2, x1, y1)))

                    elif(self.slope(line2)<0):
                        cv2.line(line_image, (x1,y1), (x2,y2), [255,100,0], 10)
                        left_lines.append((self.slope(line2), self.y_intercept(line2, x1, y1)))


        right_lane_marker = np.array([[0,0],[0,0]] ,np.int32)
        left_lane_marker = np.array([[0,0],[0,0]], np.int32)

        if len(left_lines) != 0:
            left_lines_average = np.average(left_lines, axis=0)
            left_lane_marker = self.lane_marker(image, left_lines_average)
            left_lane_marker_exists = True
            lane_markers.append(left_lane_marker)
            cv2.line(line_image, left_lane_marker[0], left_lane_marker[1], [255,0,255], 20)


        if len(right_lines) != 0:
            right_lines_average = np.average(right_lines, axis=0)
            right_lane_marker = self.lane_marker(image, right_lines_average)
            right_lane_marker_exists = True
            lane_markers.append(right_lane_marker)
            cv2.line(line_image, right_lane_marker[0], right_lane_marker[1], [0,255,0], 20)

        # if(left_lane_marker_exists and right_lane_marker_exists):
        x1 = int((right_lane_marker[0][0] + left_lane_marker[0][0]) / 2)
        x2 = int((right_lane_marker[1][0] + left_lane_marker[1][0]) / 2)
        y1 = right_lane_marker[0][1]
        y2 = right_lane_marker[1][1]
        cv2.line(line_image, (x1,y1), (x2,y2), [0,0,255], 25)
        center_line = np.array([[x1,y1,x2,y2]])


        if ("center_line" in locals()):
            return center_line, line_image, True

        else:
            print("returning default ceter line")
            return np.array([[960, 540, 960, 1080]]), line_image, False

    def slope(self, line, mode="default"):
        x1,y1,x2,y2 = line.reshape(4)

        #y = mx + b
        if(mode!="default"):
            if(x1==x2):
                x2 += 1
        m = (y2-y1)/(x2-x1)
        return m

    def y_intercept(self, line, x,y):
        #y = mx + b
        #b = y - mx
        b = y-(self.slope(line)*x)
        return b

    def lane_marker(self, image, line_params):
        try:
            slope, y_intercept = line_params.reshape(2)

            y1 = image.shape[0]
            y2 = int(y1*(2/5))
            x1 = int((y1-y_intercept)/slope)
            x2 = int((y2-y_intercept)/slope)
            return [(x1,y1),(x2,y2)]
        except:
            print("returning default lane marker")
            return [(960, 1080), (960, 792)]

    def get_points_of_line(self, line):
        x1,y1,x2,y2 = line.reshape(4)
        #https://stackoverflow.com/questions/32328179/opencv-3-0-lineiterator/59525697#59525697
        pt1 = np.array([x1,y1], np.int32)
        pt2 = np.array([x2,y2], np.int32)
        points_on_line = np.linspace(pt1, pt2, np.linalg.norm(pt1-pt2))
        return points_on_line



    def apply_input(self, slope, steering_threshold, steering_intensity):
        if(slope > steering_threshold):

            #links
            Keys.PressKey(Keys.A)
            time.sleep(steering_intensity)
            Keys.ReleaseKey(Keys.A)

        elif(slope<(-steering_threshold)):
            #rechts
            Keys.PressKey(Keys.D)
            time.sleep(steering_intensity)
            Keys.ReleaseKey(Keys.D)

    def get_file(self):
        file = filedialog.askopenfilename(filetypes=[("PNG", ".png")])
        return file

    def run(self, choice):

        steering_offset = 0 #höher: zieht nach links
        steering_intensity = 0.05 #In sekunden

        require_file = False

        if(choice=="ETS"):
            vertices = Presets.Vertices.ETS
            window_name = Presets.Windownames.ETS
            require_key_input = True

        # elif(choice=="GTA"):
        #     vertices = Presets.Vertices.GTA
        #     window_name = Presets.Windownames.GTA
        #     require_key_input = True

        elif(choice=="UNITY"):
            vertices = Presets.Vertices.UNITY
            window_name = Presets.Windownames.UNITY
            require_key_input = True
            steering_intensity = 0.07

        elif(choice=="GOOGLE"):
            vertices = Presets.Vertices.GOOGLE
            window_name = Presets.Windownames.GOOGLE
            require_key_input = False

        elif(choice=="FILE"):
            vertices = Presets.Vertices.FILE
            window_name = Presets.Windownames.FILE
            require_key_input = False
            require_file = True
            file = self.get_file()
            if(file==''):
                return



        starting_time = time.time()

        white_threshold = 100 # zwischen 0 und 255
        steering_threshold = 0.1
        found_line = False


        while(True):
            if(require_file):
                original_image = cv2.imread(file)
            else:
                original_image = self.capture_screen()
            #original_image = cv2.imread("res/Street2.png")

            original_image = cv2.resize(original_image, (0,0), fx=(float(1920/original_image.shape[1])), fy=float((1080/original_image.shape[0])))

            image = original_image
            image = self.white(image, white_threshold)
            image = self.canny(image)
            image = self.roi(image, vertices)
            image = self.stretch_image(image)
            center_line, line_image, found_line = self.lines(original_image, image)

            if (require_key_input):
                focused_window = GetWindowText(GetForegroundWindow())
                if(focused_window == window_name):
                    try:
                        road_slope = math.tan(self.slope(center_line, mode="exceptional"))
                        if(found_line):
                            road_slope += steering_offset
                            found_line = False

                        self.apply_input(road_slope, steering_threshold, steering_intensity)

                    except Exception as e:
                        print(e)

            image = self.unstretch_image(image)
            unstretched_lines = self.unstretch_image(line_image)

            #cv2.imshow("unstretched_lines", cv2.resize(unstretched_lines, (0,0), fx=0.5, fy=0.5))

            image = cv2.addWeighted(unstretched_lines, 1, original_image, 0.8, 1)

            # print("{} calculations per second" .format(1/(time.time()-starting_time)))
            # starting_time = time.time()


            #cv2.imshow("window", cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
            resized_processed = cv2.resize(image, (0,0), fx=0.5, fy=0.5)
            resized_original = cv2.resize(original_image, (0,0), fx=0.5, fy=0.5)


            cv2.imshow("processed", resized_processed)
            #cv2.imshow("original", resized_original)

            k = cv2.waitKey(33)
            if k==27:    # Esc key to stop
                cv2.destroyAllWindows()
                break

    def __init__(self):
        pass
# Leitet Programm ein
if __name__ == "__main__":
    pass
    #ir = ImageRecognition()
```

</details>

<details>

<summary>Presets.py</summary>

```python
import numpy as np

class Vertices:
    #Erstellt einen Array als Bereich
    DEFAULT = np.array([[900,450],[1100,450],[1695,850],[1000,975],[250,850]], np.int32)
    ETS = np.array([[850+17,430],[1100+34,430],[1400,675],[600-1,675]], np.int32)
    #GTA = np.array([[900,450],[1100,450],[1695,850],[1000,975],[250,850]], np.int32)
    UNITY = np.array([[900,450],[1100,450],[1695,850],[1000,975],[250,850]], np.int32)
    GOOGLE = np.array([[900,450],[1100,450],[1695,850],[1000,975],[250,850]], np.int32)
    FILE = np.array([[900,450],[1100,450],[1695,850],[1000,975],[250,850]], np.int32)

class Windownames:
    ETS = "Euro Truck Simulator 2"
    #GTA = ""
    UNITY = "SimpleCarGame"
    GOOGLE = ""
    FILE = ""
```

</details>

<details>

<summary>Keys.py</summary>

```python
import ctypes
import time
import pynput

SendInput = ctypes.windll.user32.SendInput

W = 0x11
A = 0x1E
S = 0x1F
D = 0x20


# C struct redefinitions
PUL = ctypes.POINTER(ctypes.c_ulong)
class KeyBdInput(ctypes.Structure):
    _fields_ = [("wVk", ctypes.c_ushort),
                ("wScan", ctypes.c_ushort),
                ("dwFlags", ctypes.c_ulong),
                ("time", ctypes.c_ulong),
                ("dwExtraInfo", PUL)]

class HardwareInput(ctypes.Structure):
    _fields_ = [("uMsg", ctypes.c_ulong),
                ("wParamL", ctypes.c_short),
                ("wParamH", ctypes.c_ushort)]

class MouseInput(ctypes.Structure):
    _fields_ = [("dx", ctypes.c_long),
                ("dy", ctypes.c_long),
                ("mouseData", ctypes.c_ulong),
                ("dwFlags", ctypes.c_ulong),
                ("time",ctypes.c_ulong),
                ("dwExtraInfo", PUL)]

class Input_I(ctypes.Union):
    _fields_ = [("ki", KeyBdInput),
                 ("mi", MouseInput),
                 ("hi", HardwareInput)]

class Input(ctypes.Structure):
    _fields_ = [("type", ctypes.c_ulong),
                ("ii", Input_I)]

# Actuals Functions

def PressKey(hexKeyCode):
    extra = ctypes.c_ulong(0)
    ii_ = pynput._util.win32.INPUT_union()
    ii_.ki = pynput._util.win32.KEYBDINPUT(0, hexKeyCode, 0x0008, 0, ctypes.cast(ctypes.pointer(extra), ctypes.c_void_p))
    x = pynput._util.win32.INPUT(ctypes.c_ulong(1), ii_)
    SendInput(1, ctypes.pointer(x), ctypes.sizeof(x))

def ReleaseKey(hexKeyCode):
    extra = ctypes.c_ulong(0)
    ii_ = pynput._util.win32.INPUT_union()
    ii_.ki = pynput._util.win32.KEYBDINPUT(0, hexKeyCode, 0x0008 | 0x0002, 0, ctypes.cast(ctypes.pointer(extra), ctypes.c_void_p))
    x = pynput._util.win32.INPUT(ctypes.c_ulong(1), ii_)
    SendInput(1, ctypes.pointer(x), ctypes.sizeof(x))

# directx scan codes http://www.gamespp.com/directx/directInputKeyboardScanCodes.html
```
