# Image Recognition

<p align="center">
	<img width="480" height="270" src="../Pics/result.png">
<p>

Autonomes Fahren ist eins der bewegensten Themen in der futuristischen Fortbewegungstechnologie.
Google  und Tesla investieren Summen im Milliardenbereichen in die nachhaltige Entwicklung des Autonomen Fahrens.
Wir haben uns es in diesem Projekt zur Aufgabe gemacht ein Programm zu entwickeln, welches Straßen erkennt und diese entlangfährt.
Also besteht dieses Projekt aus der Schnittstelle der Bilderkennung, Verarbeitung aber auch Auswertung.

Das Ziel soll es sein, dass das Programm auf angezeigten Straßen entlangfahren kann, also die Straßen erkennen kann.
Dies wollten wir Mithilfe von Python und der OpenCV, einer auf virtuelles Sehen und Bildverarbeitung ausgelegten Bibliothek, verwirklichen. Die Teststrecken sollen von dem Spiel Euro Truck Simulator 2 zur Verfügung gestellt werden. Im späteren Verlauf haben wir uns entschieden durch Unity eigene Teststrecken zu programmieren.

## Inhalt

- 1 [Installation](#installation)

- 2 [Benutzte Programme und Bibliotheken](#programme)

- 3 [Codeerklärung Kernprogramm](#code)

- 4 [Codeerklärung Unityspiel](#unityspiel)

- 5 [Fazit](#fazit)

- 6 [Referenzen / Weitere Literatur](#referenzen)

- 7 [Blog](#blog)

## Installation und Handbuch <a name="installation">

Das Projekt besteht aus zwei Komponenten; zum einen aus dem Programm zur Erkennung der Straße, zum anderen aus einem in Unity3D erstellten Spiel. Im Folgenden handelt es sich um die Anleitung für das Programm zur Straßenerkennung.

### Installation

Zur Installation der Erkennungssoftware [Installer.exe](../Installer.exe) aus dem Repository herunterladen und ausführen. Es öffnet sich ein Installer, bei dem der gewünschte Pfad angegben werden muss.

Nach dem Installieren kann das Programm durch öffnen der Datei ImageRecognition.exe gestartet werden.

![ImageRecognition.exe](../Pics/Installation.png)


Das Unity3D-Spiel: <https://www.mediafire.com/file/ufo96i3aqy6pgnw/Spiel.zip/file>

Um das Spiel zu starten, muss man einfach auf die <b>SimpleCarGame.exe</b> Datei drücken und es sollte gestartet werden.

### Handbuch

Durch Ausführen der Anwendung öffnet sich ein Launcher, der die Bilderkennung mit vier verschiedenen Voreinstellungen startet. Zum Auswählen auf das Bild klicken.
Die vier Modi sind:

- Der Modus Euro Truck Simulator 2, für das gleichnamige Spiel

- Der Modus Google Street View, für die Echtzeitanalyse von zum Beispiel Google Street View.

- Der Modus Unity, der für das Unity3D-Spiel ausgelegt ist.

- Der Modus File, bei dem der Benutzer ein eigenes .png-Bild analysieren lassen kann

Nach der Auswahl öffnet sich ein Fenster, das Entweder den Bildschirminhalt, oder ein analysiertes Bild anzeigt. Da das Programm den ganzen bei Windows als Hauptbildschirm eingestellten Bildschirm abfilmt, ist es erforderlich, das Fenster auf einen Zweitmonitor zu verschieben, wenn man die Echtzeitanalyse beobachten möchte.

Zum Beenden des Programms das neue Fenster fokussieren und ESCAPE drücken. Anschließend den Launcher beenden.

## Benutzte Programme und Bibliotheken <a name="programme">

### Python

<img width="250" height="100" src="../Pics/python.png">

Python ist eine Programmiersprache, die mehrere Programmiertypen unterstützt, zum Beispiel die objektorientierte, die aspektorientierte und die funktionale Programmierung. Ein großer Unterschied zu anderen Programmiersprachen ist der Verzicht auf unnötige Zeichen, so werden Blöcke nicht durch Klammern, sondern durch Einrückungen strukturiert. Es wird außerdem auf das Semikolon am Ende einer Zeile verzichtet.

Website: <https://www.python.org/>

### OpenCV

<img width="150" height="185" src="../Pics/opencv.png">

OpenCV ist eine Programmbibliothek, die auf maschinelles Sehen und Bildverarbeitung spezialisiert ist. Sie stellt die Kernfunktionen, die zur Bilderkennung vorhanden sind.

Website: <https://opencv.org/>

### Numpy

<img width="380" height="150" src="../Pics/numpy.png">

NumPy ist eine Programmbibliothek für Python, die die Handhabung mit großen mehrdimensionalen Arrays, wie zum Beispiel Vektoren, Matrizen, oder wie in unserem Fall Bildern, erleichtert. Außerdem enthält sie Funktionen für spezielle Berechnungen, die effizienter sind, als die von Python.

Website: <https://numpy.org/>

## Codeerklärung <a name="code">

Hinweis: Bei dem gezeigten Code kann es der Übersicht wegen um Ausschnitte und gekürzte Versionen handeln. Für eine nach Funktionen gegliederte Dokumentation und eine ungekürzte Version des Quelltextes [hier](Code.md) klicken.

### Main-Funktion

Der Einstieg in das Programm passiert in der Datei Window.py, in welcher sich die Main-Funktion befindet. In der Funktion wird eine neue Instanz der Klasse Window erstellt.

```python
if __name__ == "__main__":
    window = Window()
```

### Launcher

Im Konstruktor der Klasse Window wird mit der Python-Bibliothek [tkinter](https://docs.python.org/3/library/tkinter.html) ein GUI (Graphical User Interface) erstellt, welcher als eine Art Launcher für die Bilderkennungssoftware dient.

```python
    def __init__(self):

        root = tk.Tk(className=" Image Recognition")
        root.geometry('1020x600')
        root.configure(bg="black")
        root.resizable(0,0)

        #Content
        top_frame = tk.Frame(root, bg="black", width="750", height="375")
        middle_frame = tk.Frame(root,bg="black")
        bottom_frame = tk.Frame(root, bg="black")

        #Widget
        text = tk.Label(top_frame, text="|------Select preferred Game------|", font="Helvetica", bg="black", fg="white")
        link1 = tk.Label(bottom_frame, text="ImageRecognition GitLab Repository", fg="blue",bg="black", cursor="hand2")

        #Image
        ets_photo = ImageTk.PhotoImage(Image.open("res/ETSCover.jpg"))  # PIL solution
        unity_photo = ImageTk.PhotoImage(Image.open("res/UnityCover.jpg"))  # PIL solution
        google_photo = ImageTk.PhotoImage(Image.open("res/GoogleMapsLogo.jpg"))  # PIL solution
        file_photo = ImageTk.PhotoImage(Image.open("res/File.jpg"))

        #Button
        etsbutton = tk.Button(middle_frame, text="Hier drücken", image=ets_photo, command=self.onclickEts)
        unitybutton = tk.Button(middle_frame, text="Hier drücken", image=unity_photo, command=self.onclickUnity)
        googlebutton = tk.Button(middle_frame, text="Hier drücken", image=google_photo, command=self.onclickGoogle)
        filebutton = tk.Button(middle_frame, text="Hier drücken", image=file_photo, command=self.onclickFile)

        #Pack
        top_frame.grid(column=0,row=0)
        middle_frame.grid(column=0,row=1)
        bottom_frame.grid(column=0,row=2)

        link1.grid(column=1,row=3,sticky=tk.S)
        link1.bind("<Button-1>", lambda e: callback("http://www.google.com"))
        text.grid(column=0,row=1)

        etsbutton.grid(column=1,row=1)  
        unitybutton.grid(column=1,row=2)
        googlebutton.grid(column=2,row=1)
        filebutton.grid(column=2, row=2)

        root.mainloop()
        return
```

So sieht das vom Code erstellte Fenster aus:
<p align="center">
	<img width="460" height="300" src="../Pics/launcher.png">
<p>

Wird einer der vier Knöpfe gedrückt, wird eine Funktion aufgerufen, die eine Instanz der eigentlichen Kernklasse ImageRecognition erstellt. Diese Instanz wird dann durch das Aufrufen der Funktion run() gestartet, wobei die getroffene Auswahl übergeben wird.

```python
    def start_main_script(self, choice):
        ir = ImageRecognition()
        ir.run(choice)
```

### Initialisierung

Zu Beginn der Funktion run() werden je nach Auswahl verschiedene Voreinstellungen getroffen. Zum Beispiel, dass es im Modus "Euro Truck Simulator 2" erforderlich ist, Tastenanschläge zu simulieren um zu fahren. Im Datei-Modus, bei dem nur Bilder analysiert werden, ist dies nicht erforderlich, dafür muss aber eine Datei geladen werden.

```python
steering_offset = 0 # Bei einem positiven Wert zieht das Fahrzeug nach links, bei einem negativen nach rechts
steering_intensity = 0.05 # Wie lange nach links oder rechts gelenkt wird (in Sekunden)

require_file = False

if(choice=="ETS"):
    vertices = Presets.Vertices.ETS
    window_name = Presets.Windownames.ETS
    require_key_input = True

elif(choice=="UNITY"):
    vertices = Presets.Vertices.UNITY
    window_name = Presets.Windownames.UNITY
    require_key_input = True
    steering_intensity = 0.07

elif(choice=="GOOGLE"):
    vertices = Presets.Vertices.GOOGLE
    window_name = Presets.Windownames.GOOGLE
    require_key_input = False

elif(choice=="FILE"):
    vertices = Presets.Vertices.FILE
    window_name = Presets.Windownames.FILE
    require_key_input = False
    require_file = True

    file = self.get_file() # Ruft eine Funktion auf, die einen Windowsdialog aufruft, indem man eine .png Datei auswählen kann

    if(file==''):
        return

white_threshold = 75 # Wert zwischen 0 und 100, der angibt, "wie weiß" die Markierungen mindestens sein müssen
steering_threshold = 0.1 # Gibt an, wie sensibel das Programm lenkt. Ein niedrigerer Wert macht den Bot empfindlicher.
```

Um zumindest etwas Überblick zu behalten, haben wir sehr unübersichtliche Voreinstellungen in die Datei Presets.py ausgelagert.

```python
class Vertices:
    #Erstellt einen Array als Bereich
    DEFAULT = np.array([[900,450],[1100,450],[1695,850],[1000,975],[250,850]], np.int32)
    ETS = np.array([[850+17,430],[1100+34,430],[1400,675],[600-1,675]], np.int32)
    UNITY = np.array([[900,450],[1100,450],[1695,850],[1000,975],[250,850]], np.int32)
    GOOGLE = np.array([[900,450],[1100,450],[1695,850],[1000,975],[250,850]], np.int32)
    FILE = np.array([[900,450],[1100,450],[1695,850],[1000,975],[250,850]], np.int32)

class Windownames:
    ETS = "Euro Truck Simulator 2"
    UNITY = "SimpleCarGame"
    GOOGLE = ""
    FILE = ""
```

### Hauptschleife

Da das Programm Bilder in Echtzeit analysiert, muss der Code so oft und schnell wie möglich ausgeführt werden, wofür eine while-Schleife verwendet wird. In dieser befindet sich der Kern des Programms.
Wenn das Programm im Datei-Modus ausgeführt wurde, wird mithilfe des ermittelten Dateipfades das Bild geladen.

```python
while(True):
    if(require_file):
        original_image = cv2.imread(file) # Lädt die Datei
```

Für die Dokumentation dient dieses Bild als Beispiel:
<p align="center">
	<img width="930" height="540" src="../Pics/original.png">
<p>

#### Abfilmen des Bildschirmes

In allen anderen Modi muss der Bildschirm abgefilmt werden, da eine Echtzeitanalyse erfolgt.

```python
else:
    original_image = self.capture_screen()
```

Das erfolgt durch die Funktion capture_screen(), welche den Screenshot an die Schleife zurückschickt.

```python
def capture_screen(self):
    screen = np.array(ImageGrab.grab(bbox=(0,0,1920,1080)))
    screen = cv2.cvtColor(screen, cv2.COLOR_BGR2RGB) # Konvertiert von BGR zu RGB
    return screen
```

Der Screenshot wird durch die Funktion ImageGrab.grab() der Bibliothek [Pillow](https://pillow.readthedocs.io/en/stable/) erstellt. Da der Screenshot im Datenformat RGB überliefert wird und OpenCV mit BGR arbeitet, muss das Bild zu BGR konvertiert werden. Um das zu verstehen, ist es wichtig, dass man weiß wie ein Bild gespeichert wird. In diesem Fall ist das Bild ein Array (quasi eine Aufzählung) aller Pixel, wobei die Pixel aus einem Array mit 3 Werten bestehen. Einem Rot- Grün- und Blauwert zwischen 0 und 255. RGB und BGR sind im Prinzip das gleiche; der einzige Unterschied liegt darin, in welcher Reihenfolge die Werte gespeichert werden: Rot-Grün-Blau bei RGB und Blau-Grün-Rot bei BGR.
Das Bild wird im BGR-Format zurückgeschickt.

#### Weiß-Filter

In der Schleife wird nun die Funktion white() aufgerufen, wobei der Oben bestimmte Schwellenwert mitgegeben wird.

```python
image = self.white(image, white_threshold)
```

```python
    def white(self, image, threshold): #threshold = 100 (Z:132)
        #Konvertiert die Farben des übergebenen Bildes in HSV
        hsv_image = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)

        #Definieren des Bereiches der Farbe Weiß
        lower_white = np.array([0, 0, 255-threshold], dtype=np.uint8)
        upper_white = np.array([255, threshold ,255], dtype=np.uint8)
        #Nur noch Weiß erhalten + Maske auf das Bild
        mask = cv2.inRange(hsv_image, lower_white, upper_white)
        white_image = cv2.bitwise_and(image, image, mask= mask)

        white_image = cv2.cvtColor(cv2.cvtColor(white_image, cv2.COLOR_HSV2RGB),cv2.COLOR_RGB2GRAY)

        return white_image
```

Um nur die weißen Bestandteile des Bildes herauszufiltern, wird wiederum ein anderer Farbraum benötigt: HSV (Hue, Saturation, Value), weil dort alle Weißwerte relativ nah aneinander liegen. Nach dem Filtern wird das Bild in ein Graustufen-Format gebracht, weil die anderen Farbkanäle nun nicht mehr gebraucht werden und unnötige Rechenleitung verbrauchen.
Die Funktion returned die weißen Bestandteile des Bildes.

Am Beispielbild:

<p align="center">
	<img width="960" height="540" src="../Pics/white.png">
<p>

#### Canny-Filter

Der Canny-Filter ist ein Filter, der in einem Bild Kanten und Übergänge erkennt. Da das Bild im Idealfall jetzt nur noch aus der weißen Straßenmarkierung besteht, sollte der Canny-Filter diese erkennen.

```python
image = self.canny(image)
```

```python
def canny(self, image):
    image = cv2.Canny(image, 200, 600)
    return image
```

Der Algorithmus sucht im Bild nach starken Unterschieden bei zwei nebeneinanderliegenden Pixeln und gibt am Ende die Pixel zurück, bei denen er eine Kante mit einstellbarer Mindestbreite vermutet.

Am Beispiel:

<p align="center">
	<img width="930" height="540" src="../Pics/canny.png">
<p>

#### Region of Interest

Da weiße Pixel nicht nur auf der Straße, sondern überall zu finden sind, wird mit der Region of Interest (kurz: ROI) der irrelevante Teil weggeschnitten, so dass nur der Teil übrig bleibt, auf dem die Straße zu sehen ist. Dadurch werden Fehler, die zum Beipiel durch weiße Wolken verursacht werden, eingeschränkt.

```python
image = self.roi(image, vertices)
```

```python
def roi(self, image, vertices): # Region-Of-Interest
    # Maskenarray mit 0 - selbe Größe wie das Bild, welches übergeben wurde
    mask = np.zeros_like(image)
    #Ein Polygon auf der Maske der Größe Verticles mit den Farben wird erstellt
    cv2.fillPoly(mask, [vertices], [255,255,255])
    #Nur noch die Maske anzeigen
    masked_image = cv2.bitwise_and(mask, image)
    return masked_image
```

Die für die Maske verwendeten Pixel wurden vorher dem Modus angepasst. Dies ist erforderlich, da die Kameras in verschieden Programmen auf unterschiedlichen Höhen sind und mit Unterschiedlichen Winkeln zur Straße stehen.

Am Beipiel:

<p align="center">
	<img width="930" height="540" src="../Pics/roi.png">
<p>

#### Änderung der Perspektive

Um einen Blick von Oben zu "simulieren" benutzen strecken wir das Bild in y-Richtung. Dies funktioniert, weil es sich bei den Straßenmarkierungen quasi um eine zweidimensionale Linie handelt. Diese scheint länger zu sein, wenn man von Oben schaut.

```python
image = self.stretch_image(image)
```

```python
def stretch_image(self, image):
    #4 ausgewählte Positionen des Bildes = Input
    pts1 = np.array([[250,450],[1695,450],[1695,975],[250,975]], np.float32)
    #Die Punkte sollen auf diese Koordinaten verschoben werden -> Damit auch das Bild = Output
    pts2 = np.array([[0,0],[1920,0],[1920,1080],[0,1080]], np.float32)
    #Erstellt anhand der Punkte eine 2*3 Matrix
    M_perspective = cv2.getPerspectiveTransform(pts1, pts2)
    # Ändert die Perspektive mit einer 3*3 Matrix, daher ist der letzte Punkt die Ecke des Bildschirms
    image = cv2.warpPerspective(image, M_perspective, (1920,1080))

    #cv2.imshow("stretched", cv2.resize(image, (0,0), fx=0.5, fy=0.5))

    return image
```

Am Beispiel:

<p align="center">
	<img width="930" height="540" src="../Pics/stretch.png">
<p>

#### Linienerkennung mit Houghlines

Von den jetzt gestreckten Kanten der weißen Straßenmarkierungen muss das Programm jetzt Linien erkennen, um zu sehen, wo sich das Fahrzeug befindet.

```python
center_line, line_image, found_line = self.lines(original_image, image)
```

In der Funktion lines() wird zunächst mit Hilfe der Python-Bibliothek Numpy ein leeres Array mit den gleichen Maßen, (ein schwarzes Bild) wie das Inputbild erstellt. Auf diesem werden später die Linien eingezeichnet.

```python
line_image = np.zeros_like(original_image)
```

Die eigentliche Linienerkennung findet in der Funktion HoughLinesP von OpenCV statt.

```python
lines = cv2.HoughLinesP(image, 2, np.pi/180, 100, np.array([]), minLineLength=40, maxLineGap=5)
```

Bei der Geradengleichung *y=mx+b* gilt, dass jeder Punkt für den diese Gleichung erfüllt ist auf dieser Gerade liegt. Allerdings ist die gleichung nicht mehr anwendbar, wenn zwei Punkt übereinanderliegen, da sie sonst eine Steigung von Unendlich hätten. Deswegen muss man andere Parameter finden. Bei der Hough-Transformation verwendet man den Abstand zum Ursprung (d) und den Winkel zwischen der Gerade und der x-Achse (θ). Die Gleichung lautet: *d = x cos(θ) + y sin(θ)*. Auch hier trifft die Gleichung auf alle Punkte der Gerade zu. Die Funktion geht jeden als Kante erfassten Punkt durch setzt ihn in die Gleichung ein. Wenn viele Punkte die gleichen oder ähnliche Parameter haben, gibt sie den Anfangs- und Endpunkt der vermuteten Linie zurück.
Diese Linien werden von unserem Code als gültig einsortiert, wenn die Steigung zwischen zwei bestimmten Werten liegt, weil senkrechte Linien nicht nur Straße gehören können und nur ein Störfaktor sind.

```python
for line in lines:
    x1,y1,x2,y2 = line.reshape(4)
    if abs(self.slope(line)) > (1/3) and (math.isnan(self.slope(line)) is not True and (abs(self.slope(line)))<250):
        valid_lines.append(line)
```

Diese Linien werden auf ein vorläufiges Bild gemalt, welches nochmal durch den Canny-Filter geht, um schließlich nochmal von dem Houghlinesalgorithmus durchsucht zu werden. Dies garantiert eine höhrer Genauigkeit, ist allerdings sehr rechenaufwendig.

```python
for valid_line in valid_lines:
    x1,y1,x2,y2 = valid_line.reshape(4)
    cv2.line(pre_lines_image, (x1,y1), (x2,y2), [255,255,255], 1)
    pre_lines_image = self.canny(pre_lines_image)
    lines2 = cv2.HoughLinesP(pre_lines_image, 2, np.pi/180, 100, np.array([]), minLineLength=100, maxLineGap=10)
```

Anschließend werden die Linien nach nach Rechts und nach Links gerichteten Linien aufgeteilt. Dies geschieht anhand der Steigung.

```python
if(self.slope(line2)>0):
    cv2.line(line_image, (x1,y1), (x2,y2), [255,100,100], 10)
    right_lines.append((self.slope(line2), self.y_intercept(line2, x1, y1)))

elif(self.slope(line2)<0):
    cv2.line(line_image, (x1,y1), (x2,y2), [255,100,0], 10)
    left_lines.append((self.slope(line2), self.y_intercept(line2, x1, y1)))
```

Anhand der Steigung und des y-Achsenschnittpunktes wird für jede Seite jeweils eine Durchschnittslinie ermittelt und auf das bis jetzt leere Bild für die Linien gezeichnet.

```python
left_lines_average = np.average(left_lines, axis=0)
left_lane_marker = self.lane_marker(image, left_lines_average)
left_lane_marker_exists = True
lane_markers.append(left_lane_marker)
cv2.line(line_image, left_lane_marker[0], left_lane_marker[1], [255,0,255], 20)

right_lines_average = np.average(right_lines, axis=0)
right_lane_marker = self.lane_marker(image, right_lines_average)
right_lane_marker_exists = True
lane_markers.append(right_lane_marker)
cv2.line(line_image, right_lane_marker[0], right_lane_marker[1], [0,255,0], 20)
```

Aus den Werten der rechten und linken Linie wird eine Mittellinie ermittelt und gezeichnet, deren Steigung in der Theorie die Abweichung des Fahrweges angibt.

```python
x1 = int((right_lane_marker[0][0] + left_lane_marker[0][0]) / 2)
x2 = int((right_lane_marker[1][0] + left_lane_marker[1][0]) / 2)
y1 = right_lane_marker[0][1]
y2 = right_lane_marker[1][1]
cv2.line(line_image, (x1,y1), (x2,y2), [0,0,255], 25)
center_line = np.array([[x1,y1,x2,y2]])
```

Die Funktion returned drei Werte, einmal die mittlere Linie, das Bild, auf das die Linien gezeichnet wurden, und einen Boolean, der angibt, ob die mittlere Linie erfolgreich ermittelt wurde. Sollte dies nicht der Fall sein, wird eine hardgecodete Linie zurückgegeben.

```python
        if ("center_line" in locals()):
            return center_line, line_image, True

        else:
            print("returning default ceter line")
            return np.array([[960, 540, 960, 1080]]), line_image, False
```

Am Beispiel:

<p align="center">
	<img width="930" height="540" src="../Pics/lines.png">
<p>

Legende:

Blau: Von der Hough-Transformation gefundene Linien

Pink / Grün: Linke und rechte Durchschnittslinie

Rot: Mittelwert aus rechter und linker Durchschnittslinie

#### Emulation von Tastenschlägen

Zurück in der Hauptschleife überprüft der Code ob Tastenanschläge gesendet werden müssen und stellt sicher, dass dies nur passiert wenn das richtige Programm im Fokus von Windows steht. Dies ist wichtig, weil der Script sonst die ganze Zeit Buchstaben sendet, die zum Beispiel bei Texteingaben dafür sorgen, dass keine Arbeit möglich ist.

```python
if (require_key_input):
    focused_window = GetWindowText(GetForegroundWindow())
    if(focused_window == window_name):
        try:
            road_slope = math.tan(self.slope(center_line, mode="exceptional"))
            if(found_line):
                road_slope += steering_offset
                found_line = False

            self.apply_input(road_slope, steering_threshold, steering_intensity)

        except Exception as e:
            print(e)
```

In der Funktion apply_input() wird, wie der Name schon sagt, geregelt, bei welcher Steigung in welche Richtung gelenkt wird. Nachdem die Taste gedrückt wird, wird der Script für kurze Zeit gestoppt, bevor die Taste wieder losgelassen wird.

```python
def apply_input(self, slope, steering_threshold, steering_intensity):
    if(slope > steering_threshold):

        #links
        Keys.PressKey(Keys.A)
        time.sleep(steering_intensity)
        Keys.ReleaseKey(Keys.A)

    elif(slope<(-steering_threshold)):
        #rechts
        Keys.PressKey(Keys.D)
        time.sleep(steering_intensity)
        Keys.ReleaseKey(Keys.D)
```

Für die eigentliche Emulation haben wir uns für den Code [dieser](https://stackoverflow.com/questions/53643273/how-to-keep-pynput-and-ctypes-from-clashing) Seite entschieden, da andere ähnliche Ansätze nicht funktionierten. Für den Code relevant sind die Funktionen PressKey() und ReleaseKey().

```python
def PressKey(hexKeyCode):
    extra = ctypes.c_ulong(0)
    ii_ = pynput._util.win32.INPUT_union()
    ii_.ki = pynput._util.win32.KEYBDINPUT(0, hexKeyCode, 0x0008, 0, ctypes.cast(ctypes.pointer(extra), ctypes.c_void_p))
    x = pynput._util.win32.INPUT(ctypes.c_ulong(1), ii_)
    SendInput(1, ctypes.pointer(x), ctypes.sizeof(x))

def ReleaseKey(hexKeyCode):
    extra = ctypes.c_ulong(0)
    ii_ = pynput._util.win32.INPUT_union()
    ii_.ki = pynput._util.win32.KEYBDINPUT(0, hexKeyCode, 0x0008 | 0x0002, 0, ctypes.cast(ctypes.pointer(extra), ctypes.c_void_p))
    x = pynput._util.win32.INPUT(ctypes.c_ulong(1), ii_)
    SendInput(1, ctypes.pointer(x), ctypes.sizeof(x))
```

### Bildausgabe

Das Originalbild und das Bild mit den Linien werden zusammen zu einem Bild "verschmolzen".

```python
image = cv2.addWeighted(unstretched_lines, 1, original_image, 0.8, 1)
```

Dieses Bild wird auf die Hälfte geschrumpft, da es sonst zu groß wäre und ausgegeben.
```python
resized_processed = cv2.resize(image, (0,0), fx=0.5, fy=0.5)

cv2.imshow("processed", resized_processed)
```

Am Beispiel:

<p align="center">
	<img width="930" height="540" src="../Pics/result.png">
<p>

Die Schleife wird beendet, wenn Escape gedrückt wird, während das Fenster mit den Linien im Fokus ist.

```python
k = cv2.waitKey(33)
if k==27:    # Esc key to stop
    cv2.destroyAllWindows()
    break
```

Der Launcher wird dadurch nicht beendet und muss per Hand geschlossen werden.

## Unity - The Simple Car Game <a name="unityspiel">
<div align="center">

## **The Simple Car Game**

</div>

![Cover](https://gitlab.com/JKGER/imagerecognition/-/raw/Unity/Pics/TheCarGameCover.PNG)

<details>
<summary>Übersicht</summary>

  ## Allgemeines
  - [Das Spiel](#spiel)
  - [Zweck](#derzweck)
  - [Unity](#unity)
  - [Umsetzung](#umsetzung)
  - [Download](#udownload)
  - [Probleme](#uprobleme)

</details>

## Das Spiel <a name="spiel">
The Simple Car Game ist eins von uns erschaffenes Spiel, in dem der Spieler unser Programm „Imagerecognition“ testen kann. In diesem kann man unterschiedliche Autos auf verschiedenen Strecken steuern. An Startmenü wählt man sich das gewünschte Auto aus und über die Straßenauswahlknöpfe gelangt man auf die gewünschte Strecke.

Wie bei jedem handelsüblichen Autosimulator steuert man das Auto mit den Tasten W, A, S, D, bremst mit Leertaste und schaltet die Scheinwerfer mit F an oder aus. P steuert die Kameraperspektive und LShift ist gleichbedeutend mit Nitro. Wenn man genug von der Teststrecke hat, gelangt man mit der ESC-Taste zurück zum Hauptmenü.

Ein größeres Ziel verfolgt der Spieler nicht, wird also nicht abgelenkt und kann sich daher nur auf die Straße konzentrieren und seine eigenen Wege fahren.

### Inhalt
Das Spiel verfügt über 4 verschiedene Autos, wessen charakteristische Unterschiede in der Geschwindigkeit und dem Aussehen liegen. Hier sind folgende Merkmale aufgelistet:

| Aussehen | Geschwindigkeit | Bremskraft | Natürliche Bremswirkung | Lenkwinkel |
| -------- | --------------- | ---------- | ----------------------- | ---------- |
| Blau     | 200             | 400        | 100                     | 30         |
| Grau     | 400             | 500        | 200                     | 35         |
| Rot      | 500             | 600        | 250                     | 29         |
| Gelb     | 700             | 800        | 350                     | 35         |

Außerdem gibt es 4 Straßenkarten, welche sich im Relief des Terrains wie folgt unterscheiden:

| Name          | Relief                        | Besonderheiten        |
| ------------- | ----------------------------- | --------------------- |
| Infinity      | Gebirgspfad / starke Steigung | Unendlich Lang*       |
| Mountain Path | Hügelig / mittlere Steigung   | Strand / Küste        |
| Racing Road   | Eben / gerade                 | -                     |
| Plain Street  | Eben / waagerecht             | Eben für Teststrecken |

*Später mehr dazu


## Der Zweck <a name="#derzweck">
Dieses Programm haben wir entworfen, weil unser Hauptprogramm, das der Straßenerkennung, durch verschiedene Fehlerquellen verwirrt wurde und wir eben diese ausmärten wollten. Grob wiederholt orientiert sich das Programm der Imagerecognition an den weißen Punkten des aufgenommenen Bildes und errechnet anhand dieser Datensätzen den Verlauf der Straße und steuert daher diesem entlang, in der Theorie. Jedoch lenkten Leitpfosten, der Himmel (explizit die Wolken) und Objekte wie LKWs das Programm in die Irre, da diese Elemente auch weiß erscheinen und das Programm diese als Straße wahrnahm.
Daher haben wir das Spiel entworfen, welches ausschließlich weiß in der Leitlinie besaß und sonst nirgendwo. Das heißt, dass die Software nicht abgelenkt werden kann. Somit erhofften wir uns bessere Ergebnisse erzielen zu können.


## Unity <a name="#unity">
Um das Spiel zu entwickeln, haben wir uns für die Programmierumgebung namens Unity entschieden. Diese Engine machte auf uns den besten Eindruck und außerdem haben wir laut Internet erfahren, dass es für die Unity Engine am meisten Dokumentationen und Hilfeleistungen gibt. Diese waren von großer Wichtigkeit, da es unser erstes Projekt mit einer Engine war und wir daher neu und ohne Erfahrung ein neues Gebiet betraten. Zudem ist die Engine kostenlos, solange man nicht die Absicht hegt Geld zu verdienen. Weiteres wissen über das Erstellen von Terrains oder das von Autos haben wir über die Unity Dokumentation erhalten.


## Die Umsetzung <a name="#dieumsetzung">
>Vorweg soll angemerkt werden, dass das Spiel über zu große Dateien verfügt, welche es für uns unmöglich gemacht haben den Spielordner auf GitLab hochzuladen. Dennnoch haben wir das fertige Programm auf einer anderen Seite hochgeladen. Kurz: Das fertige Programm kann man sich ansehen, den Ordner des UnityEngine Programmes allerdings nicht.

Das Unityprojekt verfügt über eine übersichtliche Hierarchie:

[![](https://mermaid.ink/img/eyJjb2RlIjoiZ3JhcGggVERcbiAgQVtNZW7DvF0gLS0-fEF1dG8gdW5kIFN6ZW5lbmF1c3dhaGx8QltWZXJ0ZWlsZXJdXG4gIEIgLS0-IENbU3RyYcOfZSAxXVxuICBCIC0tPiBEW1N0cmHDn2UgMl1cbiAgQiAtLT4gRVtTdHJhw59lIG5dXG4gIEMgLS0-IHxFc2MtVGFzdGV8IEZcbiAgRCAtLT4gfEVzYy1UYXN0ZXwgRlxuICBFIC0tPiB8RXNjLVRhc3RlfCBGXG4gIEZbWnVyw7xjayB6dW0gTWVuw7xdIiwibWVybWFpZCI6eyJ0aGVtZSI6ImRlZmF1bHQifSwidXBkYXRlRWRpdG9yIjpmYWxzZX0)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggVERcbiAgQVtNZW7DvF0gLS0-fEF1dG8gdW5kIFN6ZW5lbmF1c3dhaGx8QltWZXJ0ZWlsZXJdXG4gIEIgLS0-IENbU3RyYcOfZSAxXVxuICBCIC0tPiBEW1N0cmHDn2UgMl1cbiAgQiAtLT4gRVtTdHJhw59lIG5dXG4gIEMgLS0-IHxFc2MtVGFzdGV8IEZcbiAgRCAtLT4gfEVzYy1UYXN0ZXwgRlxuICBFIC0tPiB8RXNjLVRhc3RlfCBGXG4gIEZbWnVyw7xjayB6dW0gTWVuw7xdIiwibWVybWFpZCI6eyJ0aGVtZSI6ImRlZmF1bHQifSwidXBkYXRlRWRpdG9yIjpmYWxzZX0)

Das Menü steuert welche Szene mit welchem Auto geladen wird. Die Szenen (hier gleichbedeutend mit Straßen) kann man also dynamisch nur mit einem Knopf verlinkten. Das erleichtert das spätere Hinzufügen einer neuen Straße.

Die einzelnen Straßen oder genauer die Szenen, in welchen sich die Straßen befinden, beinhalten großenteils keinen Code, da das Auto als einziges Objekt bewegt wird. Der Rest der Szene sind nur Assets, geladene Objekte, Texturen, welche kein Verhalten besitzen. Daher kommen wir nun zum wichtigsten Kernstück des Spieles und das mit dem meisten Code: Das Auto.

Wie vorhin schon erwähnt, gibt es 4 verschiedene Autos, die sich aber bei genauerer Betrachtung nur in der Textur und der Geschwindigkeit, allerdings nicht im Fahrverhalten unterscheiden. Das liegt an folgendem: Die Unity Engine ist objektorientiert und ermöglicht das Erstellen von Prefabs. Prefabs sind gespeicherte Objekte, welche im Verlaufe des Spieles häufiger instanziiert werden können. Man kann sich das ähnlich wie bei einem Bauplan vorstellen: Mit diesem Plan kann man mehrere gleiche Objekte erstellen. Dies passiert auch hier: Anfangs besaß das Spiel nur ein Auto, welches kopiert wurde, nur mit verschiedenen Eigenschaften. Daher muss man sich nur bei einem Auto genauer unter die Motorhaube gucken, um die Restlichen zu verstehen.

## Das Auto

<p align="center">
  <img width="500" height="301" src="Pics/TheCar.PNG">
</p>



Hier ist das Prefab zu erkennen. Links befindet sich die Struktur des Autos mit den einzelnen Komponenten. Das gesamte Auto besteht aus einer Ansammlung von einzelnen Objekten. Von Wichtigkeit sind allerdings nur die Wheel_Collider und die Wheel_Meshes. Der Rest erklärt sich auch von selbst.
Im Mittelpunkt ist das fertige Auto zu erkennen in der Spielumgebung. Weiterhin ist die Kamera zu erkennen, welche die Spielperspektive des Spielers repräsentiert.

Rechts befindet sich der wichtigste Reiter. Die Eigenschaften: Das Auto verfügt über einen Rigidbody und das Car Controller Script.
Der Rigidbody ist notwendig für alle physischen Objekte in der Engine, welche Masse, Reibung und Bewegung widerfahren sollten. Die Masse aller Autos ist der Einheitlichkeit auf 1500kg gestellt.

Das Car Controller Script ist das Herz der Autos. Dieses Script steuert das Verhalten anhand von Variablen und Input. Hier kommen auch die vorhin erwähnten wichtigen Wheel_Collider und Wheel_Meshes wieder ins Spiel. Wie zu erkennen ist, verfügt das Auto über 2 Achsen. Diesem wurde im Scriptinterface das dazu gehörende Objekt zugewiesen. Wheel_Collider kann man sich als imaginelle Kreise an den Rädern vorstellen, welche Kollisionen und das Fahrverhalten lenken. Wheel_Meshes sind lediglich die Radtexturen, welche vom Script gedreht werden müssen, sodass es den Eindruck erweckt, dass Auto könnte fahren.
Alle anderen sichtbaren Werte sind Variablen, welche die Beschleunigung usw. wiedergeben. Zuletzt sind auch noch die Lichter verlinkt.

<p align="center">
  <img width="500" height="301" src="Pics/thecarinterior.PNG">
</p>

Auf diesem Bild ist nochmal der Unterschied vermerkt: Der Wheel_Collider ist der dünne Kreis, der alles lenkt und der Wheel_Mesh ist das große Objekt, welches lediglich die Textur und das 3d Objekt beinhaltet.

Wenn das Auto gelenkt wird, wird das Script angesprochen:

```csharp

public List<AxleInfo> AxleInfos;
public float maxMotorTorque;
public float maxSteeringAngle;
public float brakeTourque;
public float decelerationForce;
const float gravity = 9.81f;

```

Hier sind nochmal alle im Inspektor sichtbaren Variablen zu sehen. Dabei wird diesen hier nicht ihr Wert oder Objekt zugewiesen, sondern durch den Inspektor, wie man auf dem Bild erkennen konnte.

```csharp

float motor = maxMotorTorque * Input.GetAxis("Vertical");
float steering = maxSteeringAngle * Input.GetAxis("Horizontal");

```

Motor gibt die Beschleunigung wieder und orientiet sich an der Variable und dem UserInput W oder S, wobei W = 1 und S = -1 ist. Dasselbe geschieht mit steering.
Wenn der Spieler nichts drückt, erkennt das das Script und ruft die Methode

```csharp

private void DecelerationForce(AxleInfo axleinfo)

```

auf.

Sonst wird der Wagen dementsprechend beschleunigt oder mit Leertaste gebremst.


```csharp

public void ApplyLocalPositionToVisuals(AxleInfo axleinfo)

```

Diese Methode synchronisiert den Wheel_Mesh mit dem Wheel_Collider, sodass die Textur nach der Richtung und Geschwindigkeit gedrecht wird.
Sonst ist noch wichtig zu erwähnen, dass in der Beschleunigungsmethode

```csharp

private void Acceleration(AxleInfo axleinfo, float motor)
{
    if(motor != 0)
    {
        axleinfo.leftWheel.brakeTorque = 0;
        axleinfo.rightWheel.brakeTorque = 0;
        axleinfo.leftWheel.motorTorque = motor;
        axleinfo.rightWheel.motorTorque = motor;
        if(maxSteeringAngle > 3)
        {
            maxSteeringAngle -= 0.05f;
        }
    }
    else
    {
        DecelerationForce(axleinfo);
    }
}

```

durch die Ansteigende Geschwindigkeit der Lenktwinkel reduziert wird, sodass man schwerer lenken kann, je schneller man wird. Das dient dazu, dass man nicht mehr so schnell ins Schleudern geraten kann.

Mit der GOBack Methode gelant man zurück zum Hauptmenü.
Weiters unter [Unity Engine Car Documentation](https://docs.unity3d.com/Manual/WheelColliderTutorial.html).

Auch wenn diese Seite nur das Gröbste erklärt, war das dennoch besser zu verstehen, als alle Videos. Die Wagen verfügen des Weiteren über eine Steuerblockade, die sich proportional zur Gewschwindigkeit vergrößert. Das heißt, dass desto schneller man fährt, desto schwerer kann man lenken. Dies dient der Verhinderung von Autounfällen, da die starke Lenkung zu Überschlägen führte.


## Die Szenesteuerung

<p align="center">
  <img width="500" height="301" src="Pics/TheCarSteuer.PNG">
</p>

Hier erkennt man, dass in jeder Szene die Autos geladen werden. In der Infinity-Szene, aus welcher das Bild gemacht wurde, befindet sich das Terrain und ein Objekt namens CarObject, welches als Child den CarSpawner enthält. Das CarObject deaktiviert lediglich die Kameras der Autoprefabs, sodass nicht mehrere Kameras gleichzeitig geladen werden. Das geschieht in einer Schleife, die am Start des Programmes ausgeführt wird. Diese Schleife geht Schrittweise durch die Unterobjekte durch und deaktiviert die Objekte namens Camera. Der CarSpawner spawnt das gewählte Auto beim Objekt SpawnPoint und deaktiviert alle anderen Autos. Technisch gesehen lenkt man also alle Autos, sieht sie aber nicht. Die Aktivierung des Spielerautos wird dadurch gewährleistet, dass in der MenüSzene eine globale Variable gesetzt wird mit den Previous und Next Buttons, die von dem CarSpawner aufgenommenen werden und das Auto an n-ter Stelle aktiviert wird.
Da jede Szene über den selben Aufbau verfügt, ist das CarObject die Steuerzentrale, die im Menü das Auto setzt und dieses in der nächten Szene an den CarSpawner weitergibt.

## Das Terrain
<p align="center">
  <img width="500" height="301" src="https://forum.unity.com/attachments/2019-07-03_00-48-15-gif.444764/">
</p>

Das Spiel verfügt wie schon erwähnt über 4 individuelle Terrains. Diese erstellt Unity auf Knopfdruck und die können mit Pinseln bemalt werden mit Texturen oder mit einem Pinsel nach dem Relief editiert werden. Mit dieser Methode haben wir 2 der 4 Terrains erstellt. Die anderen Beiden interessanten wurden mit

## L3DT
erstellt. Dies ist ein Programm, welches Terrains als .raw Datei generiert. Man gibt die gewünschten TerrainDaten, z.B.: Größe, Relief, Biome, ein und nach einer kurzen Dauer erstellt das Programm die Terrains. Hier sind unsere Ergebnisse:

<p align="center">
  <img width="300" height="301" src="https://gamedevacademy.org/wp-content/uploads/2017/11/l3dt_texture_map-1-300x300.png.webp">
</p>

<p align="center">
  <img width="500" height="301" src="https://gamedevacademy.org/wp-content/uploads/2020/01/img_5e0f03cc0f1ef.png.webp">
</p>


die sind so groß, dass wir zur Modellierung der Texturen und Bäume kurzerhand entschieden haben, das Terrain durch ein Script automatisch zu modifizieren. Dies ist allerdings so kompliziert und schwer, dass wir es hier nicht erklären, da es sonst den Rahmen sprengen würde. Wichtig ist nur zu erwähnen, dass das Script die Terrainsteigung an jedem Punkt misst und anhang von Schwellenwerten die Textur-Komposition errechtet und anhand der Terrainhöhe die Bäume zu einer 0,001% Change platziert. Das Erstellen des Scripts ist in solcher Maßen schwer, dass es kaum Dokumentationen darüber gibt. Unter der offiziellen Unity Dokumentation, genauer unter [Unity TerrainData](https://docs.unity3d.com/ScriptReference/TerrainData.html), ist sehr wenig zu finden. Auf anderen Seiten sind zwar Beispiele gegeben aber aufgrund von Updates sind diese veraltet und daher ungebrauchbar. Hier kann man unser Script nachlesen:

[Code](Pics/TerrainSpawner.cs)


Beim erstmaligen Starten des Scripts wird das ganze Terrain texturiert und Bäume gesetzt, sodass wir das nicht tun mussten. Im Nachhinein stimmt es zwar, dass wenn wir die Gestalltung manuell übernommen hätten, wir schneller wären, allerdings können wir in Zukunft das Script für andere Projekte recyclen. Weiterhin kann dieses Script auf seine Wünsche anpassen: Wenn man die Baumgeneration abhängig von der Steigung des Reliefs machen will, muss man nur die Steigungsvariablen eingeben. Außerdem verfügt das Script auf eine prozentuale Generation von Grass. Dieses haben wir allerdings hier deaktiviert, weil das Grass auf der Straße spawnen kann und wir daher gezwungen wären, einzelnt alle Straßen abzufahren und das Grass zu entfernen.
Somit wären die Terrains erklärt, kommen wir nun zu den:

## Straßen

Die Straßen haben wir anfangs im Unity Asset Store runtergeladen und aneinandergereiht. Das bringt allerdings die Problematik mit sich, dass diese Straßen nur 2D funktionieren, da die Assets nur für ebene Wege vorgesehen waren. Darauf haben wir alle Texturen gelöscht und sind im Internet auf [EasyRoads3d](https://assetstore.unity.com/packages/tools/terrain/easyroads3d-pro-v3-469) gestoßen. Die kostenlose Version erstellt Straßen anhand von gesetzten Punkten (Pathfinding). Mithilfe des Programmmes haben wir alle befahrbaren Straßen erstellet.

<p align="center">
  <img width="500" height="301" src="https://forum.unity.com/attachments/rterror1-gif.362794/">
</p>

## Infinity Szene
Diese Szene hat ihren Namen nicht umsonst bekommen. Hierbei haben wir uns von Tilegames inspirieren lassen und ähnlich wir in Dungeon-Spielen eine unendlich große Straße mit Terrain erstellt. Egal für wie lange man der Straße fährt, man wird nie ein Ende finden. Zwar erklärt ein Zauberer nie seine Tricks, aber so geht das von statten:
Das Terrain wurde selbstverständlich mit L3DT erstellt, nur seamless. Darunter versteht man, dass egal wie man das Terrain an sich selber angrenzen lässt, man wird niemals einen Übergang feststellen. Seamless Texturen wurden übrigens auch bei unserem erstem Projekt Lothader benutzt. Dort waren es allerdings 2d Texturen, hier sind es 3d Terrains. Dieses Terrain wurde mit unserem Script bemalt usw., und von uns mit einer Straße versehen. Diese Straße hört aber auf der genau anderen Seite mit denselben y- und z-Achsen koordinaten auf, sodass das Terrain seamless bleibt.
Jetzt ist der Trick schon fast vollbracht, aber es fehlt noch die letzte Zutat:
Das Terrain unendlich mal an sich selbst zu reihen wäre unmöglich, das Spiel würde laggen und unendlich zeitaufwendig. Daher haben wir das Terrain mit einem zweiten Script versehen, was die x-Koordinate des Spielers trackt. Wenn der Spieler 2/3 * n der Terrains hinter sicht hat, wird das Terrain nochmal instanziiert und an das erste kopiert. Der Spieler wird das nie merken und diese Schritte wiederholen sich immer wieder: Bei n + 2/3*n wird ein neues Terrain erstellt. n ist die Breite des Terrains. Das Vorherige wird selbstverständlich gelöscht.

## Probleme  <a name="uprobleme">
Probleme haben sich uns aufgestellt, als wir erkannt haben, dass die Kollisionen nicht realistisch erscheinen zwischen den Autos und Objekten. Wir haben versucht diese zu beheben, was sich allerdings als unmöglich entpuppte: Unity verlangt bei Autos einen Box-Collider, welcher das Auto auch an Ecken kollidieren lässt, bei welchen das Auto nicht ist, bzw. man darf keinen Meshcollider einstellen, der den Vorteil hat, dass er nur dort kollidiert, wo die Autotextur ist. Warum dies so ist, wissen wir nicht und neben dem Box-Collider sind alle weiteren Alternativen  weniger realistisch.

## Fazit <a name="fazit">

Die Bilderkennung bei stehenden Bildern funktioniert oft, wie gewünscht, wenn die Bilder nach Bedacht ausgewählt werden. Die weißen Linien müssen am besten durchgezogen in großen Kontrast zur Fahrbahnoberfläche stehen. Deshalb funktioniert der Code auch zum Beispiel auf Autobahnen nicht.
Bei der Echtzeitanalyse kam zuletzt das Problem auf, dass das Programm so rechenaufwendig wurde, dass das Spiel Euro Truck Simulator 2 aufgrund mangelnder Ressourcen nicht mehr flüssig lief. Zudem ist der Code sehr fehleranfällig und kann zum Beispiel nicht die weißen Pfosten am Straßenrand erkennen und hält sie für die Markierung.

Als zweiköpfiges Programmierteam und kommen wir mit unserem Ergebnis natürlich nicht annähernd an millionenschwere Unternehmen wie Google oder Tesla heran. Trotzdem sind wir mit unserer Arbeit zufrieden.

## Referenzen / Weitere Literatur <a name="referenzen">

[Sentdex - Python plays GTA](https://pythonprogramming.net/game-frames-open-cv-python-plays-gta-v/)

[OpenCV Docs - Canny](https://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/canny_detector/canny_detector.html)

[OpenCV Docs - Hough-Transformation](https://docs.opencv.org/3.4/d9/db0/tutorial_hough_lines.html)

[Wikipedia - Hough-Transformation](https://de.wikipedia.org/wiki/Hough-Transformation)

## Blog <a name="blog">

### 4.12.19

Am ersten neuem Projekttag haben wir das Konzept unseres Projektes entworfen und ein provisorisches GitLab Repository erstellt. Das Projekt haben wir ImageRecognition getauft und von da an tagtäglich erweitert.
Visual Studio Code haben wir auf den Schulrechner installiert sowie auf den eigenen Laptops, wenn es noch nicht installiert war. Darauf haben wir angefangen, die Addons hinzuzufügen.

### 5.12.19

Am darauf folgendem Schultag wurden die Extentions vollständig heruntergeladen, unter anderem für die Straßenerkennung wichtige Bibliotheken wie Python Pillow. Die ersten Codezeilen wurden zudem verfasst.

### 10.12.19

OpenCV, eine freie Programmbibliothek, wurde installiert, da diese für die Bildaufnahme, Verarbeitung und das maschinelle Sehen verantwortlich ist. Aufgrund dessen bildet diese Bibliothek das Fundament unserer darauffolgender Arbeit und beeinflusst daher die Qualität unseres Produktes.
Zuhause wurde der Code erweitert und stetig mit dem GitLab Repository abgeglichen.

### 11.12.19

Die Schule besaß Internetprobleme, weshalb wir freibekommen haben und daher nichts produktives geleistet haben (in der Info-Stunde).

### 12.12.19

Die Internetprobleme wurden zwar noch nicht behoben, allerdings konnten wir die ersten Testläufe unseres Programmes an von Google Maps heruntergeladenen Bildern Testen.
Wichtig ist hierführ anzumerken, dass es sich lediglich um stationäre Bilder handelt und nicht um Bewegbilder, was nochmal ein gewaltigen Unterschied darstellt.
Also haben wir die Stunde damit verbracht in England die Straßen rauf und runter zu fahren und den Output unseres Programmes zu überprüfen.

### 18.12.19

Wir haben mit der Dokumentation begonnen, erste Texte verfasst und das Projekt strukturiert. Bewusst haben wir uns diesmal dafür entschieden, das Wiki in der Readme Datei anzusiedeln, da die Idee des Gebrauches der von GitLab bereit gestellten Anwendung des Wikis im letzten Projekt nicht positiv ankam.

### 19.12.19

Unser Projekt nimmt allmählich an Form an. Die Straßenerkennung funktioniert teilweise, wenn auch mit leichten Fehlern. Google Maps Bilder wertet das Programm
vollständig aus. Weiterhin wurden Issues für die spätere Bearbeitung geöffnet.

### 14.01.20

Von Zuhause haben wir an dem Projekt weitergearbeitet und die Schulstude dafür genutzt, den Code der Straßenerkennungssoftware anfänglich zu erläutern. Die Startmethoden, der grobe Aufbau und unser Konzpet wurden verfasst und in die Readme hinzugefügt.

### 15.01.20

Um den Code verständlich erläutern du können haben wir Bilder der Straßenerkennung aufgenommen und gespeichert, sodass wir im Nachhinein unsere Erklärungen bildlich hervorheben können. Danach haben wir Issues auf der Seite GitLab erstellt, sodass wir später von Zuhause aus die nächsten Aufgaben in Blick haben können und diese Punkt für Punkt abarbeiten können.
Bis jetzt funktioniert das Programm grob, da es durch unverhinderbare Objekte im Spiel irritiert wird.

### 16.01.20

Wir haben uns weiter auf das Entwickeln des Kerncodes konzentriert und diesen versucht zu verbessern. Um den Benutzungsbereich des Programmes zu erhöhen, vesuchten wir weiterhin die Software kompatibel für andere Spiele zu machen. GTA oder ein Unity-Projekt sollen auch benutzbar sein.

### 22.01.20

Da das Steuern von Fahrzeugen in Computerspielen normalerweise durch die Tastatur erfolgt, haben wir angefangen nach einer Lösung zu suchen, mit der man aus dem Script heraus virtuelle Tastenanschläge an Windows schicken kann. Diese werden vom Spiel als "normale" Tastenanschläge auf der Tastatur wahrgenommen und das Fahrzeug bewegt sich dementsprechend. Vorerst hat unsere Lösung jedoch nicht funktioniert.

### 23.01.20

Wir haben weiterhin versucht, eine Lösung zu finden, mit der es zuverlässig gelingt, Tastatursanschläge zu verwenden.
Zudem haben wir ein weiteres Script erstellt, welches am Start des Programmes ein Fenster öffnet, in welchem der Endnutzer seine gewünschte Umgebung auswählen kann. Zur Auswahl gehören zurzeit Google Maps, ETS, GTA und Unity.
Weiterhin haben wird die Hintergrundfarbe des Fensters festgelegt.

#### 28.01.20

Nach langer Recherche sind wir auf eine [Lösung](https://stackoverflow.com/questions/53643273/how-to-keep-pynput-and-ctypes-from-clashing) gestoßen, mit der man eine Tastatur zuverlässig emulieren kann. Das Fenster sollte unterhalb der Auswahl einen Link zum GitLab Repository besitzen, wessen Einbeziehung in das Script sich als erstaunlich schwieriger entpuppte hatte als gedacht.

### 29.01.20

Wir haben etwa die Hälfte der Zeit damit verbracht, den noch sehr fehleranfälligen Code zu Optimieren. Die andere Hälfte haben wir damit verbracht, an der Dokumentation zu arbeiten.

### 30.01.20

Es hat kein Unterricht stattgefunden.

### 4.02.20

Wir haben beschlossen eine Art Launcher für unseren Code zu programmieren, der den Code mit den für das jeweilige Programm benötigten Parametern (z.B. die Lenkintensität) ausführt.  Dieses erledigen wir mit der Bibliothek TkInter. Dieser Launcher soll des weiteren über das vorher erstellte Interface mit der Programmauswahl verfügen. Desweiteren haben wir am Blog weitergearbeitet.

### 5.02.20

Der Launcher funktioniert jetzt, wir haben allerdings noch graphische Elemente hinzugefügt, um ihn ansprechender zu gestalten sowie das Fenster neu skaliert. Ab sofort sind große Bilder erkennbar und wenn man versucht das ohnehin vollkommen ausreichend große Fenster zu vergrößern, bleiben die Interfaceelemente zentral gelegen und ordnen sich nach den Skalierungsfaktoren an. Außerdem haben wir angefangen erste Testläufe mit dem Spiel "Euro Truck Simulator 2" zu machen.

### 6.02.20

Durch die Testläufe in "Euro Truck Simulator 2" konnten wir auf Fehlerquellen schließen. Zum Beispiel kann das Programm nicht zwischen Leitlinien und Leitpfosten unterscheiden, da beide weiß sind und somit die Software diese als Straße wahrnimmt, weshalb der Wagen in die falsche Richtung gelenkt wird.

### 12.02.20

Der Launcher funktioniert jetzt. Klickt der Benutzer auf das Symbol für das gewünschte Programm, wird der Code der Straßenerkennung mit auf das Programm angepassten Parametern ausgeführt.
Um bekannte Fehlerquellen zu umgehen, haben wir beschlossen ein simples Spiel mit Unity3D zu entwickeln, in dem wir zum Beispiel auf die Leitpfosten verzichten. Ein eigenes Programm hat den Vorteil, dass wir die volle Kontrolle über die Umwelt haben.

### 13.02.20

Die Entwicklung eines solchen Spieles stellt sich zunächst schwieriger heraus, als gedacht, weshalb wir uns auf Fehlersuche begeben haben. Das Spiel ist oftmals gecrasht. Außerdem wurde viel Zeit dafür investiert, ein fahrtüchtiges Auto zu erstellen. Unity besitzt war eine Dokumentation darüber, diese ist allerdings nur auf das Minimalste, betreffend der Informationen, beschränkt.

### 14.02.20

Wir haben überlegt, ob wir im weiteren Verlauf der Entwicklungsphase die bekannte KI-Bibliothek TensorFlow in unseren Code zu integrieren. Dazu haben wir drei Artikel gefunden:

- <https://towardsdatascience.com/deep-learning-for-self-driving-cars-7f198ef4cfa2#>
- <https://towardsdatascience.com/deeppicar-part-1-102e03c83f2c>
- <https://github.com/Project-Road-Sign-Detection/Tensorflow-Street-Sign-Recognition>

### 19.02.20

Erstmal haben wir ohne Integration eines Neuralen Netzwerks sowohl am Kernprogramm, als auch am Unity3D Programm weitergearbeitet. Dabei haben wir uns vor allem auf das Terrain fokussiert und dessen Generation optimiert.

### 20.02.20

An diesem Tag hat der Eine am ImageRecognition Projekt weitergearbeitet, während der Andere sich darüber informiert hat, wie man besser Straßen in Unity verlegt, anstatt einzelne Segmente aneinanderzureihen, was extrem zeitaufwendig ist und sehr schwer umsetzbar für Wege mit Steigungen ist. Daher haben wir die Zeit damit genutzt auf dem Euro Truck Simulator die Erkennungssoftware die Straßen entlang zu fahren und im Internet nach Alternativen gesucht, wobei wir fündig wurden auf das Projekt: [EasyRoads3D](https://assetstore.unity.com/packages/tools/terrain/easyroads3d-pro-v3-469). Davon die kostenlose Version, verständlich.

### 11.03.20

Hier wurde erstmals ImageRecognition auf den Straßen des "The Simple Car Games" getestet. Diese Tests fielen allerdings negativ aus. Nach längerem Debuggen haben wir entdeckt, dass das Spiel über zu viele Störfaktoren verfügt, welche umgehend eliminiert werden müssen. Einerseits wurde der weiß / helle Himmel als Straße erkannt, andererseit die Leitlinie und Richtungslinie nicht erkannt. Das liegt daran, dass das Eine zu hell, das Andere zu dunkel ist. Die übriggebliebene Zeit konnten wir nicht mehr dazu nutzen um dies zu beheben, also haben wir das von Zuhause aus gemacht.

### 13.03.20

Heute wird vorraussichtlich einer der letzten Schultage vor Abgabe des Projektes sein, da der Coronavirus sich innerhalb Deutschlands ausbreitet. Daher haben wir die letzte Stunde dafür genutzt die Kompatibilität unseres Unity Spiels mit der Straßenerkennungssoftware zu testen. Durch diese Testläufe haben wir erfahren, dass "The Simple Car Game" eine weitere Kameraeinstellung benötigt sowie eine anpassbare Scheinwerferlichtintensivität. Diese Modifikationen haben wir versucht noch möglichst schnell umzusetzen, wobei wir auf Fehler beim Unityspiel stießen. Diese wurden erst von Zuhause aus entfernt.
